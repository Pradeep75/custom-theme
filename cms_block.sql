-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 28, 2017 at 12:00 PM
-- Server version: 10.1.10-MariaDB
-- PHP Version: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tiles_magento_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `cms_block`
--

CREATE TABLE `cms_block` (
  `block_id` smallint(6) NOT NULL COMMENT 'Block ID',
  `title` varchar(255) NOT NULL COMMENT 'Block Title',
  `identifier` varchar(255) NOT NULL COMMENT 'Block String Identifier',
  `content` mediumtext COMMENT 'Block Content',
  `creation_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Block Creation Time',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Block Modification Time',
  `is_active` smallint(6) NOT NULL DEFAULT '1' COMMENT 'Is Block Active'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='CMS Block Table';

--
-- Dumping data for table `cms_block`
--

INSERT INTO `cms_block` (`block_id`, `title`, `identifier`, `content`, `creation_time`, `update_time`, `is_active`) VALUES
(1, 'Footer Links Block', 'footer_links_block', '<div class="footer_nav">\r\n					     <ul>\r\n						     <li><a href="">Shop</a></li>\r\n							 <li><a href="">Contact</a></li>\r\n							 <li><a href="">About</a></li>\r\n							 <li><a href="">Delivery</a></li>\r\n							 <li><a href="">Terms & Conditions</a></li>\r\n						 </ul>\r\n				     </div>', '2017-01-12 05:54:17', '2017-01-14 11:44:23', 1),
(2, 'Header Contact us info', 'header-contact-us-info', '<ul>\r\n                    	<li><span class="bold_ext">Contact Us</span></li>\r\n                        <li>Call: 01392 275906</li>\r\n                    </ul>', '2017-01-12 05:54:17', '2017-01-14 10:13:57', 1),
(4, 'Footer map address', 'map-address', '<div class="order_part col-lg-6">\r\n<div class="order_part_inner_box">\r\n<div class="dilivery_cont">ORDER FOR DELIVERY <img class="shipping-truck" src="{{view url="images/shipping_truck.png"}}" alt="" width="58" height="42" /></div>\r\n<div class="address">\r\n<h4>How to find us</h4>\r\n<p>47 Marsh Green Rd, Exeter EX2 8PN isales@scabags.co.uk | 01392 275906</p>\r\n</div>\r\n<div class="locaton_cont"><img class="icon-pic" src="{{view url="images/map_pin.png"}}" alt="" width="30" height="47" /> ORDER FOR DELIVERY</div>\r\n</div>\r\n</div>\r\n<div class="map_part col-lg-6">\r\n<div class="map_part_inner"><iframe style="border: 0;" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2526.751130909527!2d-3.5331395847244353!3d50.70600267679967!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x486da69cf87ec76b%3A0x56a4273aeaf29413!2s47+Marsh+Green+Rd+W%2C+Exeter+EX2+8PN%2C+UK!5e0!3m2!1sen!2sin!4v1484717266187" width="551" height="350" frameborder="0" allowfullscreen=""></iframe> <!--img src="{{view url="images/map.png"}}" alt="" /--></div>\r\n</div>', '2017-01-12 05:54:18', '2017-01-18 05:37:16', 1),
(5, 'Footer Contact address and social icon', 'footer-contact-adress', '<div class="footer-col col-lg-3">\r\n<div class="footer_logo"><img src="{{view url="images/footer_logo.png"}}" alt="" /></div>\r\n</div>\r\n<div class="footer-col col-lg-3">\r\n<p>47 Marsh Green Rd, <br />Exeter EX2 8PN <br />isales@scabags.co.uk <br />01392 275906</p>\r\n<div class="social_icon">\r\n<ul>\r\n<li><a><img src="{{view url="images/facebook.png"}}" /></a></li>\r\n</ul>\r\n</div>\r\n</div>', '2017-01-12 05:54:18', '2017-01-14 12:00:16', 1),
(14, 'Home Page Block', 'home-page-block', '<div class="blocks-promo">\n   <a href="{{store url=""}}collections/yoga-new.html" class="block-promo home-main">\n       <img src="{{media url="wysiwyg/home/home-main.jpg"}}" alt="" />\n       <span class="content bg-white">\n           <span class="info">New Luma Yoga Collection</span>\n           <strong class="title">Get fit and look fab in new seasonal styles</strong>\n           <span class="action more button">Shop New Yoga</span>\n       </span>\n   </a>\n   <div class="block-promo-wrapper block-promo-hp">\n       <a href="{{store url=""}}promotions/pants-all.html" class="block-promo home-pants">\n           <img src="{{media url="wysiwyg/home/home-pants.jpg"}}" alt="" />\n           <span class="content">\n               <strong class="title">20% OFF</strong>\n               <span class="info">Luma pants when you shop today*</span>\n               <span class="action more icon">Shop Pants</span>\n           </span>\n       </a>\n       <a href="{{store url=""}}promotions/tees-all.html" class="block-promo home-t-shirts">\n           <span class="image"><img src="{{media url="wysiwyg/home/home-t-shirts.png"}}" alt="" /></span>\n           <span class="content">\n               <strong class="title">Even more ways to mix and match</strong>\n               <span class="info">Buy 3 Luma tees get a 4th free</span>\n               <span class="action more icon">Shop Tees</span>\n           </span>\n       </a>\n       <a href="{{store url=""}}collections/erin-recommends.html" class="block-promo home-erin">\n           <img src="{{media url="wysiwyg/home/home-erin.jpg"}}" alt="" />\n           <span class="content">\n               <strong class="title">Take it from Erin</strong>\n               <span class="info">Luma founder Erin Renny shares her favorites!</span>\n               <span class="action more icon">Shop Erin Recommends</span>\n           </span>\n       </a>\n       <a href="{{store url=""}}collections/performance-fabrics.html" class="block-promo home-performance">\n           <img src="{{media url="wysiwyg/home/home-performance.jpg"}}" alt="" />\n           <span class="content bg-white">\n               <strong class="title">Science meets performance</strong>\n               <span class="info">Wicking to raingear, Luma covers&nbsp;you</span>\n               <span class="action more icon">Shop Performance</span>\n           </span>\n       </a>\n       <a href="{{store url=""}}collections/eco-friendly.html" class="block-promo home-eco">\n           <img src="{{media url="wysiwyg/home/home-eco.jpg"}}" alt="" />\n           <span class="content bg-white">\n               <strong class="title">Twice around, twice as nice</strong>\n               <span class="info">Find conscientious, comfy clothing in our <nobr>eco-friendly</nobr> collection</span>\n               <span class="action more icon">Shop Eco-Friendly</span>\n           </span>\n       </a>\n   </div>\n</div>\n<div class="content-heading">\n   <h2 class="title">Hot Sellers</h2>\n   <p class="info">Here is what`s trending on Luma right now</p>\n</div>\n{{widget type="Magento\\\\CatalogWidget\\\\Block\\\\Product\\\\ProductsList" products_per_page="8" products_count="8" template="product/widget/content/grid.phtml" conditions_encoded="a:2:[i:1;a:4:[s:4:`type`;s:50:`Magento|CatalogWidget|Model|Rule|Condition|Combine`;s:10:`aggregator`;s:3:`all`;s:5:`value`;s:1:`1`;s:9:`new_child`;s:0:``;]s:4:`1--1`;a:4:[s:4:`type`;s:50:`Magento|CatalogWidget|Model|Rule|Condition|Product`;s:9:`attribute`;s:3:`sku`;s:8:`operator`;s:2:`()`;s:5:`value`;s:60:`WS12, WT09, MT07, MH07, 24-MB02, 24-WB04, 241-MB08, 240-LV05`;]]"}}\n', '2017-01-12 05:54:21', '2017-01-12 05:54:21', 1),
(17, 'Expert text', 'expert-text', '<div class="top-dark_ext">Experts in Packaging <span>since 1976</span></div>', '2017-01-12 05:54:21', '2017-01-14 10:20:13', 1),
(18, 'Login Info Block', 'login-data', '<div class="message info" style="margin-top: 50px;">\n    <p><strong>Try Demo Customer Access</strong></p>\n    <p><span style="display:inline-block; width: 80px; padding-right: 10px;">Email:</span>roni_cost@example.com</p>\n    <p><span style="display:inline-block; width: 80px; padding-right: 10px;">Password:</span>roni_cost3@example.com</p>\n</div>', '2017-01-12 05:54:21', '2017-01-12 05:54:21', 1),
(19, 'footer newsletter subscribe', 'news-letter', '{{block class="Magento\\Newsletter\\Block\\Subscribe" name="static.newsletter" template="Magento_Newsletter::subscribe.phtml"}}', '2017-01-18 05:13:59', '2017-01-18 05:13:59', 1),
(20, 'Delivery info', 'delivery_info', '<p>Delivery Info</p>', '2017-01-18 12:36:49', '2017-01-18 12:36:49', 1),
(21, 'Featured producs for product pages', 'featured_products', '{{widget type="Magento\\CatalogWidget\\Block\\Product\\ProductsList" title="featured Products list" products_count="4" template="product/widget/content/grid.phtml" conditions_encoded="a:2:[i:1;a:4:[s:4:`type`;s:50:`Magento|CatalogWidget|Model|Rule|Condition|Combine`;s:10:`aggregator`;s:3:`all`;s:5:`value`;s:1:`1`;s:9:`new_child`;s:0:``;]s:4:`1--1`;a:4:[s:4:`type`;s:50:`Magento|CatalogWidget|Model|Rule|Condition|Product`;s:9:`attribute`;s:12:`category_ids`;s:8:`operator`;s:2:`==`;s:5:`value`;s:2:`41`;]]"}}', '2017-01-27 08:12:14', '2017-01-27 08:14:38', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cms_block`
--
ALTER TABLE `cms_block`
  ADD PRIMARY KEY (`block_id`);
ALTER TABLE `cms_block` ADD FULLTEXT KEY `CMS_BLOCK_TITLE_IDENTIFIER_CONTENT` (`title`,`identifier`,`content`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cms_block`
--
ALTER TABLE `cms_block`
  MODIFY `block_id` smallint(6) NOT NULL AUTO_INCREMENT COMMENT 'Block ID', AUTO_INCREMENT=22;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
