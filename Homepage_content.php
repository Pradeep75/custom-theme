<div class="row">{{block class="Magento\Framework\View\Element\Template" template="Magento_Theme::html/categoryList.phtml"}}
<div class="col-lg-9  responsive-banner">
<div class="right_banner_section">{{block class="Magestore\Bannerslider\Block\SliderItem" name="bannerslider.slidercustom" slider_id="1"}}</div>
</div>
</div>
<div class="row">
<div class="midd_shippin_outer_box">
<div class="col-lg-3 map_part">
<div class="map-btn"><img class="map-icon-pic" src="{{view url="images/map_pin.png"}}" alt="" width="30" height="47" />ORDER FOR <br />COLLECTION</div>
</div>
<div class="col-lg-6 business_ext">
<div class="business_ext_ex">Convenient for your business</div>
</div>
<div class="col-lg-3 shiping_part">
<div class="order_dilivery"><img class="shipping-truck" src="{{view url="images/shipping_truck.png"}}" alt="" width="58" height="42" />ORDER <br />FOR DELIVERY</div>
</div>
</div>
</div>
<div class="row">
<div class="product_slider col-lg-12">
<h3>Feaured Products</h3>
{{widget type="Magento\CatalogWidget\Block\Product\ProductsList" title="Featured" products_count="4" template="product/widget/content/grid.phtml" conditions_encoded="a:2:[i:1;a:4:[s:4:`type`;s:50:`Magento|CatalogWidget|Model|Rule|Condition|Combine`;s:10:`aggregator`;s:3:`all`;s:5:`value`;s:1:`1`;s:9:`new_child`;s:0:``;]s:4:`1--1`;a:4:[s:4:`type`;s:50:`Magento|CatalogWidget|Model|Rule|Condition|Product`;s:9:`attribute`;s:12:`category_ids`;s:8:`operator`;s:2:`==`;s:5:`value`;s:2:`41`;]]"}}</div>
</div>
<div class="row">
<div class="packing_specility col-lg-12">
<div class="col-lg-4">
<div class="logo_w_car"><img src="{{view url='images/logo_w_car.png'}}" alt="" /></div>
</div>
<div class="col-lg-8">
<h3>Packaging specialists since 1976</h3>
<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto.</p>
<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate</p>
</div>
</div>
</div>